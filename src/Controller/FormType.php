<?php

namespace App\Controller;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class FormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $deDatumVoorDeform = $options['data']['deDatumVoorDeForm'];
        $startDatum = $options['data']['startDatum'];

        $builder
            ->setMethod('GET')
            ->add(
                'datumRange',
                ChoiceType::class,
                [
                    'label' => 'Uren in de periode',
                    'data_class' => UrenController::class,
                    'choices' => $deDatumVoorDeform,
                    'data' => $startDatum,
                    'data_class' => null,
                ]
            )
            ->add(
                'gesorteerdOp',
                ChoiceType::class,
                [
                    'label' => ' Gesorteerd op ',
                    'choices' => [
                        'Ticket ID' => 'ticket',
                        'Uren' => 'uren',
                        'Soort' => 'soort',
                        'Status' => 'status',
                        'Titel' => 'title',
                    ],
                    'data' => 'ticket'
                ]
            );
    }
}