<?php

namespace App\Controller;

use App\Repository\UrenverantwoordingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UrenController extends AbstractController
{
    /**
     * @Route("/", name="app_uren")
     */
    public function UrenVerantwoording(UrenverantwoordingRepository $repository, Request $request)
    {
        $startDatum = $this->datumRangeNu();

        $datums = $this->datumRange();
        $deDatumVoorDeform = $datums['AlleDatums'];

        $formFilterOpSoort = $this->createForm(FormType::class,
            [
                'deDatumVoorDeForm' => $deDatumVoorDeform,
                'startDatum' => $startDatum,
            ]);

        $formFilterOpSoort->handleRequest($request);

        $datumSoorteren = $this->datumRangeGeselecteerd(
            $formFilterOpSoort->get('datumRange')->getData()
        );

        $deData = $repository->kiesDeQuery($formFilterOpSoort->get('gesorteerdOp')->getData(), $datumSoorteren);

        $datumOpvragen = $this->datumRangeGeselecteerd($formFilterOpSoort->get('datumRange')->getData());
        $deDatumVanForm = $datumOpvragen['0'].' t/m ' . $datumOpvragen['1'];
        $gesorteerdop = $formFilterOpSoort->get('gesorteerdOp')->getData();

        $deTotaleTijd = $this->urenOmzetten($deData);

        $factuurTijdBerekenen = $this->urenOpFactuur($deTotaleTijd);

        return $this->render('urenverantwoording.html.twig', [
            'form' => $formFilterOpSoort->createView(),
            'resultaat' => $deData,
            'totaal' => $deTotaleTijd,
            'factuurTijd' => $factuurTijdBerekenen,
            'gesorteerd' => $gesorteerdop,
            'datumGesorteerd' => $deDatumVanForm,
        ]);
    }

    /**
     * @param $deTijd
     * @return int|string
     */
    private function urenOpFactuur($deTijd)
    {
        // Conversie naar minuten
        $deTijd /= 60;

        // Afronden op 5 minuten
        $deTijd = ceil($deTijd / 5) * 5;

        // Conversie naar uren / aantallen
        $deTijd /= 60;

        // Afronden op .25
        $deTijd = ceil($deTijd * 4) / 4;

        $deTijd = str_replace('.', ',', $deTijd);

        return $deTijd;
    }

    /**
     * @param array $uren
     * @return array
     */
    private function urenOmzetten(array $uren)
    {
        return array_sum(array_column($uren, 'totaalUrenId'));

    }

    /**
     * @return array
     */
    private function datumRange()
    {
        $datum = "2016-01-01";
        $huidigeJaar = date("Y-m-d", strtotime('Dec 31'));
        $value = 0;

        $alleDatum = [];
        $datumVanEnTot = [];

        for ($i = $datum; $i <= $huidigeJaar;) {
            //hier bereken ik de date automatisch
            $data = date("Y-m-d", strtotime($i . "+ 14 days"));
            $datum2 = date("Y-m-d", strtotime($data . "+ 1day "));
            $data2 = date("Y-m-d", strtotime("last day of" . $i));

            //De eerste helft van de maand wordt in de array datumVanEnTot gezet.
            $datumVanEnTot[$value]['van'] = $i;
            $datumVanEnTot[$value]['tot'] = $data;

            //Hier maak 1 hele label van die in de form wordt getoont
            $bijelkaar = $i . " t/m " . $data;
            $bijelkaar2 = $datum2 . " t/m " . $data2;

            //hier zet ik hem in de array alleDatum de eerste helft
            $alleDatum[$bijelkaar] = $value;

            //de value +1
            $value++;

            //De tweede helft van de maand in de array datumVanEnTot zetten.
            $datumVanEnTot[$value]['van'] = $datum2;
            $datumVanEnTot[$value]['tot'] = $data2;

            //hier zet ik hem in de array alleDatum de tweede helft
            $alleDatum[$bijelkaar2] = $value;
            $value++;

            //Hier mee ga ik naar de volgende maand.
            $i = date("Y-m-d", strtotime($i . "+ 1 month"));
        }

        return [
            'AlleDatums' => $alleDatum,
            'datumVanEnTot' => $datumVanEnTot
        ];
    }

    /**
     * @param int $selectedDatumRangeInt
     *
     * @return array
     */
    private function datumRangeGeselecteerd($datumGeselecteerd)
    {
        $deDatumRange = $this->datumRange();
        $datumsVanEnTot = $deDatumRange['datumVanEnTot'];

        return [
            $datumsVanEnTot[$datumGeselecteerd]['van'],
            $datumsVanEnTot[$datumGeselecteerd]['tot']
        ];
    }

    private function datumRangeNu()
    {
        $deDatumRange = $this->datumRange();


        if ((date('d')) <= 15) {
            $datum1 = date("Y-m-d", strtotime("first day of this month"));
            $datum2 = date("Y-m-d", strtotime($datum1."+ 14 days"));
        }

        if ((date('d')) >= 16) {

            $datum = date("Y-m-d", strtotime("first day of this month"));
            $datum1 = date("Y-m-d", strtotime($datum."+ 15 days"));
            $datum2 = date("Y-m-d", strtotime("last day of this month"));
        }

        $bijelkaar = $datum1 .' t/m '.$datum2;

        $alleDatumRange = $deDatumRange['AlleDatums'][$bijelkaar];

        return $alleDatumRange;

    }
}