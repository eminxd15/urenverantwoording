<?php

namespace App\Repository;

use App\Entity\Tickets;
use App\Entity\Urenverantwoording;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Urenverantwoording|null find($id, $lockMode = null, $lockVersion = null)
 * @method Urenverantwoording|null findOneBy(array $criteria, array $orderBy = null)
 * @method Urenverantwoording[]    findAll()
 * @method Urenverantwoording[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrenverantwoordingRepository extends ServiceEntityRepository
{

    public $order;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Urenverantwoording::class);
    }

    public function soortOp($value)
    {
        switch ($value){
            case 'ticket':
            case 'uren':
                return 'u';
                break;

            case 'soort':
            case 'status':
            case 'title':
                return 'ticket';
                break;
        }
    }

    public function kiesDeQuery($order,$deDatumRange)
    {
        $datum1 = $deDatumRange['0'];
        $datum2 = $deDatumRange['1'];
        return $this->createQueryBuilder('u')
            ->select([
                'IDENTITY(u.ticket) AS ticketId',
                'SUM(TIMETOSEC(u.uren)) AS totaalUrenId',
                'ticket.soort',
                'ticket.status',
                'ticket.title',
                'ticket.url'])
            ->leftJoin('u.ticket', 'ticket')
            ->where("u.datum BETWEEN '$datum1' AND '$datum2'")
            ->groupBy('u.ticket')
            ->orderBy($this->soortOp($order).'.'.$order,'ASC')
            ->getQuery()
            ->getResult();
    }


    // /**
    //  * @return Urenverantwoording[] Returns an array of Urenverantwoording objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Urenverantwoording
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
