<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('bereken_uren', [$this, 'berekenUren'])
        ];
    }

    public function berekenUren($uren)
    {
        $uur = floor($uren / 3600);
        $minute = floor($uren % 3600) / 60;

        return sprintf('%02s:%02s', $uur, $minute);
    }
}