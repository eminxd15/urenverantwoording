<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Systemmedewerkers
 *
 * @ORM\Table(name="Systemmedewerkers")
 * @ORM\Entity
 */
class Systemmedewerkers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Voornaam", type="string", length=11, nullable=true)
     */
    private $voornaam = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="Achternaam", type="string", length=11, nullable=true)
     */
    private $achternaam = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="pkMedewerkersId", type="integer", nullable=true)
     */
    private $pkmedewerkersid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVoornaam(): ?string
    {
        return $this->voornaam;
    }

    public function setVoornaam(?string $voornaam): self
    {
        $this->voornaam = $voornaam;

        return $this;
    }

    public function getAchternaam(): ?string
    {
        return $this->achternaam;
    }

    public function setAchternaam(?string $achternaam): self
    {
        $this->achternaam = $achternaam;

        return $this;
    }

    public function getPkmedewerkersid(): ?int
    {
        return $this->pkmedewerkersid;
    }

    public function setPkmedewerkersid(?int $pkmedewerkersid): self
    {
        $this->pkmedewerkersid = $pkmedewerkersid;

        return $this;
    }


}
