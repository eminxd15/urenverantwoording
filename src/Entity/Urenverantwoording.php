<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Urenverantwoording
 *
 * @ORM\Table(name="urenverantwoording", indexes={@ORM\Index(name="isBetaald", columns={"isBetaald"}), @ORM\Index(name="fkRelatieId", columns={"fkRelatieId"}), @ORM\Index(name="isVrijeDag", columns={"isVrijeDag"}), @ORM\Index(name="isDeclarabel", columns={"isDeclarabel"}), @ORM\Index(name="fkProjectId", columns={"fkProjectId"}), @ORM\Index(name="fk_urenverantwoording_medewerkers1", columns={"fkMedewerkersId"})})
 * @ORM\Entity(repositoryClass="App\Repository\UrenverantwoordingRepository")
 */
class Urenverantwoording
{
    /**
     * @var int
     *
     * @ORM\Column(name="pkUrenverantwoordingId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pkurenverantwoordingid;

    /**
     * @var int
     *
     * @ORM\Column(name="fkRelatieId", type="integer", nullable=false)
     */
    private $fkrelatieid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="fkProjectId", type="integer", nullable=true)
     */
    private $fkprojectid = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="isBetaald", type="integer", nullable=false)
     */
    private $isbetaald = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="isDeclarabel", type="integer", nullable=false)
     */
    private $isdeclarabel = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="medewerkerBehandeld", type="integer", nullable=true)
     */
    private $medewerkerbehandeld;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datumBehandeld", type="datetime", nullable=true)
     */
    private $datumbehandeld;

    /**
     * @var int
     *
     * @ORM\Column(name="isVrijeDag", type="integer", nullable=false)
     */
    private $isvrijedag = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum", type="date", nullable=false)
     */
    private $datum;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="uren", type="time", nullable=false)
     */
    private $uren;

    /**
     * @var string
     *
     * @ORM\Column(name="omschrijving", type="text", length=0, nullable=false)
     */
    private $omschrijving;

    /**
     * @var Tickets
     *
     * @ORM\ManyToOne(targetEntity="Tickets")
     * @ORM\JoinColumn(name="ticketid",referencedColumnName="hash")
     */
    private $ticket;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumAangepast", type="datetime", nullable=false)
     */
    private $datumaangepast;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumToegevoegd", type="datetime", nullable=false)
     */
    private $datumtoegevoegd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="evenementNaam", type="string", length=255, nullable=true)
     */
    private $evenementnaam;

    /**
     * @var float|null
     *
     * @ORM\Column(name="kostenTickets", type="float", precision=10, scale=0, nullable=true)
     */
    private $kostentickets;

    /**
     * @var float|null
     *
     * @ORM\Column(name="kostenReizen", type="float", precision=10, scale=0, nullable=true)
     */
    private $kostenreizen;

    /**
     * @var float|null
     *
     * @ORM\Column(name="kostenOverige", type="float", precision=10, scale=0, nullable=true)
     */
    private $kostenoverige;

    /**
     * @var float|null
     *
     * @ORM\Column(name="kostenVerblijf", type="float", precision=10, scale=0, nullable=true)
     */
    private $kostenverblijf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="kostenOmschrijving", type="text", length=0, nullable=true)
     */
    private $kostenomschrijving;

    /**
     * @var \Systemmedewerkers
     *
     * @ORM\ManyToOne(targetEntity="Systemmedewerkers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fkMedewerkersId", referencedColumnName="id")
     * })
     */
    private $fkmedewerkersid;

    public function getPkurenverantwoordingid(): ?int
    {
        return $this->pkurenverantwoordingid;
    }

    public function getFkrelatieid(): ?int
    {
        return $this->fkrelatieid;
    }

    public function setFkrelatieid(int $fkrelatieid): self
    {
        $this->fkrelatieid = $fkrelatieid;

        return $this;
    }

    public function getFkprojectid(): ?int
    {
        return $this->fkprojectid;
    }

    public function setFkprojectid(?int $fkprojectid): self
    {
        $this->fkprojectid = $fkprojectid;

        return $this;
    }

    public function getIsbetaald(): ?int
    {
        return $this->isbetaald;
    }

    public function setIsbetaald(int $isbetaald): self
    {
        $this->isbetaald = $isbetaald;

        return $this;
    }

    public function getIsdeclarabel(): ?int
    {
        return $this->isdeclarabel;
    }

    public function setIsdeclarabel(int $isdeclarabel): self
    {
        $this->isdeclarabel = $isdeclarabel;

        return $this;
    }

    public function getMedewerkerbehandeld(): ?int
    {
        return $this->medewerkerbehandeld;
    }

    public function setMedewerkerbehandeld(?int $medewerkerbehandeld): self
    {
        $this->medewerkerbehandeld = $medewerkerbehandeld;

        return $this;
    }

    public function getDatumbehandeld(): ?\DateTimeInterface
    {
        return $this->datumbehandeld;
    }

    public function setDatumbehandeld(?\DateTimeInterface $datumbehandeld): self
    {
        $this->datumbehandeld = $datumbehandeld;

        return $this;
    }

    public function getIsvrijedag(): ?int
    {
        return $this->isvrijedag;
    }

    public function setIsvrijedag(int $isvrijedag): self
    {
        $this->isvrijedag = $isvrijedag;

        return $this;
    }

    public function getDatum(): ?\DateTimeInterface
    {
        return $this->datum;
    }

    public function setDatum(\DateTimeInterface $datum): self
    {
        $this->datum = $datum;

        return $this;
    }

    public function getUren(): ?\DateTimeInterface
    {
        return $this->uren;
    }

    public function setUren(\DateTimeInterface $uren): self
    {
        $this->uren = $uren;

        return $this;
    }

    public function getOmschrijving(): ?string
    {
        return $this->omschrijving;
    }

    public function setOmschrijving(string $omschrijving): self
    {
        $this->omschrijving = $omschrijving;

        return $this;
    }

    public function getTicket(): ?Tickets
    {
        return $this->ticket;
    }

    public function setTicket(Tickets $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }

    public function getDatumaangepast(): ?\DateTimeInterface
    {
        return $this->datumaangepast;
    }

    public function setDatumaangepast(\DateTimeInterface $datumaangepast): self
    {
        $this->datumaangepast = $datumaangepast;

        return $this;
    }

    public function getDatumtoegevoegd(): ?\DateTimeInterface
    {
        return $this->datumtoegevoegd;
    }

    public function setDatumtoegevoegd(\DateTimeInterface $datumtoegevoegd): self
    {
        $this->datumtoegevoegd = $datumtoegevoegd;

        return $this;
    }

    public function getEvenementnaam(): ?string
    {
        return $this->evenementnaam;
    }

    public function setEvenementnaam(?string $evenementnaam): self
    {
        $this->evenementnaam = $evenementnaam;

        return $this;
    }

    public function getKostentickets(): ?float
    {
        return $this->kostentickets;
    }

    public function setKostentickets(?float $kostentickets): self
    {
        $this->kostentickets = $kostentickets;

        return $this;
    }

    public function getKostenreizen(): ?float
    {
        return $this->kostenreizen;
    }

    public function setKostenreizen(?float $kostenreizen): self
    {
        $this->kostenreizen = $kostenreizen;

        return $this;
    }

    public function getKostenoverige(): ?float
    {
        return $this->kostenoverige;
    }

    public function setKostenoverige(?float $kostenoverige): self
    {
        $this->kostenoverige = $kostenoverige;

        return $this;
    }

    public function getKostenverblijf(): ?float
    {
        return $this->kostenverblijf;
    }

    public function setKostenverblijf(?float $kostenverblijf): self
    {
        $this->kostenverblijf = $kostenverblijf;

        return $this;
    }

    public function getKostenomschrijving(): ?string
    {
        return $this->kostenomschrijving;
    }

    public function setKostenomschrijving(?string $kostenomschrijving): self
    {
        $this->kostenomschrijving = $kostenomschrijving;

        return $this;
    }

    public function getFkmedewerkersid(): ?Systemmedewerkers
    {
        return $this->fkmedewerkersid;
    }

    public function setFkmedewerkersid(?Systemmedewerkers $fkmedewerkersid): self
    {
        $this->fkmedewerkersid = $fkmedewerkersid;

        return $this;
    }


}
