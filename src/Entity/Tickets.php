<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tickets
 *
 * @ORM\Table(name="tickets", indexes={@ORM\Index(name="subtaskRight", columns={"subtaskRight"}), @ORM\Index(name="parent", columns={"parent"}), @ORM\Index(name="subtaskOf", columns={"subtaskOf"}), @ORM\Index(name="subtaskDepth", columns={"subtaskDepth"}), @ORM\Index(name="subtaskLeft", columns={"subtaskLeft"})})
 * @ORM\Entity(repositoryClass="App\Repository\ticketRepository")
 */
class Tickets
{
    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $hash = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="kayakoId", type="integer", nullable=true)
     */
    private $kayakoid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="soort", type="string", length=255, nullable=true)
     */
    private $soort;

    /**
     * @var string|null
     *
     * @ORM\Column(name="parent", type="string", length=255, nullable=true)
     */
    private $parent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subtaskOf", type="string", length=255, nullable=true)
     */
    private $subtaskof;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="medewerker", type="string", length=255, nullable=true)
     */
    private $medewerker;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var int|null
     *
     * @ORM\Column(name="projectType", type="integer", nullable=true)
     */
    private $projecttype;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateAdded", type="datetime", nullable=true)
     */
    private $dateadded;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateChanged", type="datetime", nullable=true)
     */
    private $datechanged;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="followup", type="datetime", nullable=true)
     */
    private $followup;

    /**
     * @var string|null
     *
     * @ORM\Column(name="geschat", type="string", length=255, nullable=true)
     */
    private $geschat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gewerkt", type="string", length=255, nullable=true)
     */
    private $gewerkt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="geboekt", type="string", length=255, nullable=true)
     */
    private $geboekt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="subtaskLeft", type="integer", nullable=true)
     */
    private $subtaskleft;

    /**
     * @var int|null
     *
     * @ORM\Column(name="subtaskRight", type="integer", nullable=true)
     */
    private $subtaskright;

    /**
     * @var int|null
     *
     * @ORM\Column(name="subtaskDepth", type="integer", nullable=true)
     */
    private $subtaskdepth;

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function getKayakoid(): ?int
    {
        return $this->kayakoid;
    }

    public function setKayakoid(?int $kayakoid): self
    {
        $this->kayakoid = $kayakoid;

        return $this;
    }

    public function getSoort(): ?string
    {
        return $this->soort;
    }

    public function setSoort(?string $soort): self
    {
        $this->soort = $soort;

        return $this;
    }

    public function getParent(): ?string
    {
        return $this->parent;
    }

    public function setParent(?string $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getSubtaskof(): ?string
    {
        return $this->subtaskof;
    }

    public function setSubtaskof(?string $subtaskof): self
    {
        $this->subtaskof = $subtaskof;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getMedewerker(): ?string
    {
        return $this->medewerker;
    }

    public function setMedewerker(?string $medewerker): self
    {
        $this->medewerker = $medewerker;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getProjecttype(): ?int
    {
        return $this->projecttype;
    }

    public function setProjecttype(?int $projecttype): self
    {
        $this->projecttype = $projecttype;

        return $this;
    }

    public function getDateadded(): ?\DateTimeInterface
    {
        return $this->dateadded;
    }

    public function setDateadded(?\DateTimeInterface $dateadded): self
    {
        $this->dateadded = $dateadded;

        return $this;
    }

    public function getDatechanged(): ?\DateTimeInterface
    {
        return $this->datechanged;
    }

    public function setDatechanged(?\DateTimeInterface $datechanged): self
    {
        $this->datechanged = $datechanged;

        return $this;
    }

    public function getFollowup(): ?\DateTimeInterface
    {
        return $this->followup;
    }

    public function setFollowup(?\DateTimeInterface $followup): self
    {
        $this->followup = $followup;

        return $this;
    }

    public function getGeschat(): ?string
    {
        return $this->geschat;
    }

    public function setGeschat(?string $geschat): self
    {
        $this->geschat = $geschat;

        return $this;
    }

    public function getGewerkt(): ?string
    {
        return $this->gewerkt;
    }

    public function setGewerkt(?string $gewerkt): self
    {
        $this->gewerkt = $gewerkt;

        return $this;
    }

    public function getGeboekt(): ?string
    {
        return $this->geboekt;
    }

    public function setGeboekt(?string $geboekt): self
    {
        $this->geboekt = $geboekt;

        return $this;
    }

    public function getSubtaskleft(): ?int
    {
        return $this->subtaskleft;
    }

    public function setSubtaskleft(?int $subtaskleft): self
    {
        $this->subtaskleft = $subtaskleft;

        return $this;
    }

    public function getSubtaskright(): ?int
    {
        return $this->subtaskright;
    }

    public function setSubtaskright(?int $subtaskright): self
    {
        $this->subtaskright = $subtaskright;

        return $this;
    }

    public function getSubtaskdepth(): ?int
    {
        return $this->subtaskdepth;
    }

    public function setSubtaskdepth(?int $subtaskdepth): self
    {
        $this->subtaskdepth = $subtaskdepth;

        return $this;
    }


}
